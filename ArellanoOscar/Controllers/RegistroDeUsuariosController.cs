﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Domain;

namespace ArellanoOscar.Controllers
{
    public class RegistroDeUsuariosController : ApiController
    {
        public int RegistroUsuario(int IdEmpresa, UsuariosModel Usuario)
        {
            UsuariosDomain usuario = new UsuariosDomain();
            Usuario.IdEmpresa = IdEmpresa;
            int Exito = usuario.GuardaUsuario(Usuario);

            return Exito;
        }
    }
}
