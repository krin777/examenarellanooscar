﻿using Domain;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ArellanoOscar.Controllers
{
    public class EditarUsuarioController : ApiController
    {
        public int EditarUsuario(int IdEmpresa, UsuariosModel Usuario)
        {
            UsuariosDomain usuario = new UsuariosDomain();
            Usuario.IdEmpresa = IdEmpresa;
            int Exito = usuario.EditarUsuario(Usuario);

            return Exito;
        }
    }
}
