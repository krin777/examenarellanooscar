﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ArellanoOscar.Controllers
{
    public class RecuperarContrasenaController : ApiController
    {
        public string RecuperarContrasena(int IdEmpresa, int IdUsuario)
        {
            UsuariosDomain usuario = new UsuariosDomain();
            string Contrasena = usuario.RecuperarContrasena(IdEmpresa, IdUsuario);

            return Contrasena;
        }
    }
}
