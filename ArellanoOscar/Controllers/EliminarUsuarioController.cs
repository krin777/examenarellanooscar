﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ArellanoOscar.Controllers
{
    public class EliminarUsuarioController : ApiController
    {
        public int EliminarUsuario(int IdEmpresa, int IdUsuario)
        {
            UsuariosDomain usuario = new UsuariosDomain();
            int Exito = usuario.EliminarUsuario(IdEmpresa, IdUsuario);

            return Exito;
        }
    }
}
