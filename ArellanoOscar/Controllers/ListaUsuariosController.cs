﻿using Domain;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ArellanoOscar.Controllers
{
    public class ListaUsuariosController : ApiController
    {
        public List<ListaUsuarios> ListaUsuario(int IdEmpresa)
        {
            UsuariosDomain usuario = new UsuariosDomain();
            List<ListaUsuarios> ListaUsuario = new List<ListaUsuarios>();
            ListaUsuario = usuario.ObtieneUsuarios(IdEmpresa).ListaUsuarios;

            return ListaUsuario;
        }
    }
}
