﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicioRosFer.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace Domain
{
    public class UsuariosDomain
    {
        private DataAccessSqlServer dao;
        private const string Con = "conexionBD";

        public int GuardaUsuario(UsuariosModel Usuario)
        {
            int Exito = 0;
            DataSet tablas = new DataSet();

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter() { ParameterName = "@p_IdEmpresa", Value = Usuario.IdEmpresa });
            param.Add(new SqlParameter() { ParameterName = "@p_IdUsuario", Value = Usuario.IdUsuario });
            param.Add(new SqlParameter() { ParameterName = "@p_NombreUsuario", Value = Usuario.NombreUsuario });
            param.Add(new SqlParameter() { ParameterName = "@p_ApePaterno", Value = Usuario.ApePaterno });
            param.Add(new SqlParameter() { ParameterName = "@p_ApeMaterno", Value = Usuario.ApeMaterno});
            param.Add(new SqlParameter() { ParameterName = "@p_Usuario", Value = Usuario.Usuario });
            param.Add(new SqlParameter() { ParameterName = "@p_Contrasena", Value = Usuario.Contrasena });

            using (dao = new DataAccessSqlServer(Con))
            {
                tablas = dao.ExecuteDataSet("Usuario.SP_RegistroUsuario", param);
            }

            if (tablas.Tables != null && tablas.Tables[0] != null && tablas.Tables[0].Rows.Count > 0)
            {
                DataRow fila = tablas.Tables[0].Rows[0];
                Exito = fila.Field<int>("Exito");
            }

            return Exito;
        }

        public int EditarUsuario(UsuariosModel Usuario)
        {
            int Exito = 0;
            DataSet tablas = new DataSet();

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter() { ParameterName = "@p_IdEmpresa", Value = Usuario.IdEmpresa });
            param.Add(new SqlParameter() { ParameterName = "@p_IdUsuario", Value = Usuario.IdUsuario });
            param.Add(new SqlParameter() { ParameterName = "@p_NombreUsuario", Value = Usuario.NombreUsuario });
            param.Add(new SqlParameter() { ParameterName = "@p_ApePaterno", Value = Usuario.ApePaterno });
            param.Add(new SqlParameter() { ParameterName = "@p_ApeMaterno", Value = Usuario.ApeMaterno });
            param.Add(new SqlParameter() { ParameterName = "@p_Usuario", Value = Usuario.Usuario });
            param.Add(new SqlParameter() { ParameterName = "@p_Contrasena", Value = Usuario.Contrasena });

            using (dao = new DataAccessSqlServer(Con))
            {
                tablas = dao.ExecuteDataSet("Usuario.SP_EditarUsuario", param);
            }

            if (tablas.Tables != null && tablas.Tables[0] != null && tablas.Tables[0].Rows.Count > 0)
            {
                DataRow fila = tablas.Tables[0].Rows[0];
                Exito = fila.Field<int>("Exito");
            }

            return Exito;
        }

        public int EliminarUsuario(int IdEmpresa, int IdUsuario)
        {
            int Exito = 0;
            DataSet tablas = new DataSet();

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter() { ParameterName = "@p_IdEmpresa", Value = IdEmpresa });
            param.Add(new SqlParameter() { ParameterName = "@p_IdUsuario", Value = IdUsuario });

            using (dao = new DataAccessSqlServer(Con))
            {
                tablas = dao.ExecuteDataSet("Usuario.SP_EliminarUsuario", param);
            }

            if (tablas.Tables != null && tablas.Tables[0] != null && tablas.Tables[0].Rows.Count > 0)
            {
                DataRow fila = tablas.Tables[0].Rows[0];
                Exito = fila.Field<int>("Exito");
            }

            return Exito;
        }

        public UsuariosModel ObtieneUsuarios(int IdEmpresa)
        {
            UsuariosModel response = new UsuariosModel();
            DataSet tablas = new DataSet();
            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter() { ParameterName = "@p_IdEmpresa", Value = IdEmpresa });

            using (dao = new DataAccessSqlServer(Con))
            {
                tablas = dao.ExecuteDataSet("Produccion.SP_ObtieneUsuarios", param);
            }

            if (tablas.Tables != null && tablas.Tables[0] != null && tablas.Tables[0].Rows.Count > 0)
            {
                List<ListaUsuarios> usuario = tablas.Tables[0].AsEnumerable().Select(m => new ListaUsuarios()
                {
                    IdEmpresa = m.Field<int>("IdEmpresa"),
                    IdUsuario = m.Field<int>("IdUsuario"),
                    NombreUsuario = m.Field<string>("NombreUsuario"),
                    ApePaterno = m.Field<string>("ApePaterno"),
                    ApeMaterno = m.Field<string>("ApeMaterno"),
                    Usuario = m.Field<string>("Usuario"),
                    Contrasena = m.Field<string>("Contrasena")
                }).ToList();
                response.ListaUsuarios = usuario;
            }
            else
            {
                response.ListaUsuarios = new List<ListaUsuarios>();
            }

            return response;
        }

        public string RecuperarContrasena(int IdEmpresa, int IdUsuario)
        {
            string Contrasena = "";
            DataSet tablas = new DataSet();

            List<SqlParameter> param = new List<SqlParameter>();
            param.Add(new SqlParameter() { ParameterName = "@p_IdEmpresa", Value = IdEmpresa });
            param.Add(new SqlParameter() { ParameterName = "@p_IdUsuario", Value = IdUsuario });

            using (dao = new DataAccessSqlServer(Con))
            {
                tablas = dao.ExecuteDataSet("Usuario.SP_ObtieneContrasena", param);
            }

            if (tablas.Tables != null && tablas.Tables[0] != null && tablas.Tables[0].Rows.Count > 0)
            {
                DataRow fila = tablas.Tables[0].Rows[0];
                Contrasena = fila.Field<string>("Contrasena");
            }

            return Contrasena;
        }
    }
}
