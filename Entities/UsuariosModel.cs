﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class UsuariosModel
    {
        public int IdEmpresa { get; set; }
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public List<ListaUsuarios> ListaUsuarios { get; set; }
    }

    public class ListaUsuarios
    {
        public int IdEmpresa { get; set; }
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
    }
}
